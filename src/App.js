function Square({ value }) {
  return <button className="square">{value}</button>;
}
//value prop allows us to assign each square with a value when we call the function in the Board function below
export default function Board() {
  return (
    <>
      <div className="board-row">
        <Square value="1" />
        <Square value="2" />
        <Square value="3" />
      </div>
      <div className="board-row">
        <Square value="4" />
        <Square value="5" />
        <Square value="6" />
      </div>
      <div className="board-row">
        <Square value="7" />
        <Square value="8" />
        <Square value="9" />
      </div>
    </>
  );

// In each row there is an assigned square declared by the value that is selected
// each time the square component is called.
}
//This component is a piece of resuable code that represents a part of a user interface (UI)
//Components are used to render, manage, and update the UI elements in your application.
